#include <iostream>
#include <cmath>


using namespace std;

//one way
int reverse(int num)
{
	
	int r = 0, multiply = 0;
	while (num > 0)
	{
		multiply = (int)log10(num); // calculate how many 10 to the powers in num
		r += (num%10)* pow(10, multiply);
		num = num/10;
	}

	return r;

}

//more efficient way
int reverse2(int num)
{
	
	int r = 0;
	while (num > 0)
	{
		r = r*10 + (num%10);
		num = num/10;
	}

	return r;

}


int main (int argc, char** argv) {

	int x = 0;
	cout <<"Enter the input ";
	cin >> x;
	cout  << "output " << reverse2(x) << endl; 
  	
  	return 0;
}